import firebase from 'firebase'
import firestore from 'firebase/firestore'


// Initialize Firebase
var config = {
    apiKey: "AIzaSyAhBKruDbiI7U_7EIuW_NcjO8VuJN1EGuQ",
    authDomain: "udemy-ninja-smoothies-35494.firebaseapp.com",
    databaseURL: "https://udemy-ninja-smoothies-35494.firebaseio.com",
    projectId: "udemy-ninja-smoothies-35494",
    storageBucket: "udemy-ninja-smoothies-35494.appspot.com",
    messagingSenderId: "802494802712"
  };
 
 const firebaseApp = firebase.initializeApp(config);

 firebaseApp.firestore().settings({timestampsInSnapshots: true})

//export firestore database
export default firebaseApp.firestore();

